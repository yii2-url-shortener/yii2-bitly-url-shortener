<?php

namespace mikk150\urlshortener\bitly;

use mikk150\urlshortener\ShortenerInterface;
use yii\authclient\InvalidResponseException;
use yii\authclient\OAuth2;
use yii\base\Component;
use yii\base\ErrorHandler;
use yii\di\Instance;
use yii\helpers\ArrayHelper;

class Shortener extends Component implements ShortenerInterface
{
    /**
     * @var BitlyOauth2|array|string oauth2 client configuration
     */
    public $oauth2;

    /**
     * @var ErrorHandler|array|string Errorhandler via witch it will report errors
     */
    public $errorHandler = 'errorHandler';

    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->oauth2 = Instance::ensure($this->oauth2, OAuth2::class);

        $this->errorHandler = Instance::ensure($this->errorHandler, ErrorHandler::class);
    }

    /**
     * Sortens given URL
     *
     * @param $url
     * @return string
     */
    public function shorten($url)
    {
        try {
            return ArrayHelper::getValue($this->oauth2->api('shorten', 'POST', [
                'long_url' => $url
            ]), 'link', false);
        } catch (InvalidResponseException $exception) {
            $this->errorHandler->handleException($exception);

            return false;
        }
    }
}
