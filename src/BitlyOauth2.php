<?php

namespace mikk150\urlshortener\bitly;

use yii\authclient\OAuth2;

class BitlyOauth2 extends OAuth2
{
    /**
     * {@inheritdoc}
     */
    public $authUrl = 'https://bitly.com/oauth/authorize';

    /**
     * {@inheritdoc}
     */
    public $tokenUrl = 'https://bitly.com/oauth/access_token';

    /**
     * {@inheritdoc}
     */
    public $apiBaseUrl = 'https://api-ssl.bitly.com/v4';

   /**
     * {@inheritdoc}
     */
    protected function initUserAttributes()
    {
        return $this->api('user', 'GET');
    }

    /**
     * {@inheritdoc}
     */
    public function applyAccessTokenToRequest($request, $accessToken)
    {
        $request->getHeaders()->set('Authorization', 'Bearer ' . $accessToken->getToken());
    }

    /**
     * {@inheritdoc}
     */
    protected function defaultName()
    {
        return 'bitly';
    }

    /**
     * {@inheritdoc}
     */
    protected function defaultTitle()
    {
        return 'Bitly';
    }
}
