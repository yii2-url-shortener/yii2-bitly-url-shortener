<?php

namespace tests\unit;

use Codeception\Test\Unit;
use mikk150\urlshortener\bitly\BitlyOauth2;
use yii\authclient\OAuthToken;
use yii\httpclient\Client;

class BitlyOauth2Test extends Unit
{
    public function testDefaultName()
    {
        $oauth2 = new BitlyOauth2;
        $this->assertEquals('bitly', $oauth2->getName());
    }

    public function testDefaultTitle()
    {
        $oauth2 = new BitlyOauth2;
        $this->assertEquals('Bitly', $oauth2->getTitle());
    }

    public function testInitUser()
    {
        $oauth2 = $this->make(BitlyOauth2::class, [
            'api' => function ($subUrl, $method, $data = []) {
                $this->assertEquals('user', $subUrl);
                $this->assertEquals('GET', $method);
                return [
                    'default_group_guid' =>  'string',
                    'name' =>  'string',
                    'created' =>  'string',
                    'is_active' =>  true,
                    'modified' =>  'string',
                    'is_sso_user' =>  true,
                    'is_2fa_enabled' =>  true,
                    'login' =>  'string',
                    'emails' =>  [
                        [
                            'is_primary' =>  true,
                            'is_verified' =>  true,
                            'email' =>  'string'
                        ]
                    ]
                ];
            }
        ]);

        $this->assertEquals([
            'default_group_guid' =>  'string',
            'name' =>  'string',
            'created' =>  'string',
            'is_active' =>  true,
            'modified' =>  'string',
            'is_sso_user' =>  true,
            'is_2fa_enabled' =>  true,
            'login' =>  'string',
            'emails' =>  [
                [
                    'is_primary' =>  true,
                    'is_verified' =>  true,
                    'email' =>  'string'
                ]
            ]
        ], $oauth2->getUserAttributes());
    }
    
    public function testApplyAccessTokenToRequest()
    {
        $oauth2 = new BitlyOauth2;
        $request = $oauth2->createRequest();

        $token = new OAuthToken([
            'token' => 'test'
        ]);

        $oauth2->applyAccessTokenToRequest($request, $token);

        $this->assertEquals('Bearer test', $request->getHeaders()->get('Authorization'));
    }
}
