<?php

namespace tests\unit;

use Codeception\Test\Unit;
use mikk150\urlshortener\bitly\BitlyOauth2;
use mikk150\urlshortener\bitly\Shortener;
use yii\authclient\InvalidResponseException;
use yii\console\ErrorHandler;
use yii\httpclient\Response;

class ShortenerTest extends Unit
{
    public function testShortenerSucceedsToShortenUrl()
    {
        $shortener = new Shortener([
            'oauth2' => $this->make(BitlyOauth2::class, [
                'api' => function ($subUrl, $method = 'GET', $data = []) {
                    $this->assertEquals('shorten', $subUrl);
                    $this->assertEquals('POST', $method);
                    $this->assertEquals(['long_url' => 'http://neti.ee'], $data);

                    return ['link' => 'http://example.com'];
                },
            ]),
            'errorHandler' => $this->makeEmpty(ErrorHandler::class),
        ]);

        $this->assertEquals('http://example.com', $shortener->shorten('http://neti.ee'));
    }

    public function testShortenerFailsToShortenUrl()
    {
        $shortener = new Shortener([
            'oauth2' => $this->make(BitlyOauth2::class, [
                'api' => function ($subUrl, $method = 'GET', $data = []) use (&$shortener) {
                    $response = $shortener->oauth2->getHttpClient()->createResponse('Not found', [
                        'HTTP/1.1 404 Not Found'
                    ]);
                    throw new InvalidResponseException($response);
                }
            ]),
            'errorHandler' => $this->makeEmpty(ErrorHandler::class),
        ]);

        $this->assertFalse($shortener->shorten('http://neti.ee'));
    }
}
